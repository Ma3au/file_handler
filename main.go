package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"sync/atomic"
	"sync"
)

// Функция, которая принимает на вход строку и переводит её в int64
func ConvertStringToInt64(s string) int64 {
	num, err := strconv.Atoi(s)
	if err != nil {
		fmt.Println(err)
		fmt.Printf("This number will not be included in the total: %s\n", string(s))
		num = 0
	}

	return int64(num)
}

func main() {
    	var wg sync.WaitGroup
	// Количество запускаемых горутин
	var numberOfGoroutines = 5

	var sum int64 = 0

	// Создаем канал с токенами по количеству запускаемых горутин
	free := make(chan struct{}, numberOfGoroutines)
	for i := 0; i < numberOfGoroutines; i++ {
		free <- struct{}{}
	}

	// Открываем файл для чтения
	file, err := os.Open("file.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	// Запускаем сканнер, сканируем асинхронно файл построчно, берем токен, переводим число из строки в int64
	// и прибавляем атомарно к sum, затем возвращаем токен в канал
	scanner := bufio.NewScanner(file)
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	for scanner.Scan() {
	    wg.Add(1)
	    go func() {
	        defer wg.Done()
    		line := scanner.Text()
    
    		<-free
    
    		go func() {
    			atomic.AddInt64(&sum, ConvertStringToInt64(line))
    			free <- struct{}{}
    		}()
	    }()
	    
	    wg.Wait()
	}

	// Дожидаемся завершения работы всех горутин, закрываем канал и выводим результат
	for i := 0; i < numberOfGoroutines; i++ {
		<-free
	}
	close(free)

	fmt.Println(sum)
}
