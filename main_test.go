package main

import (
	"testing"
)

func TestConvertStringToInt64(t *testing.T) {
	// Тест с корректным числом
	s := "123"
	expected := int64(123)
	got := ConvertStringToInt64(s)
	if got != expected {
		t.Errorf("Expected %d, but got %d", expected, got)
	}

	// Тест с некорректным числом
	s = "abcd"
	expected = 0
	got = ConvertStringToInt64(s)
	if got != expected {
		t.Errorf("Expected %d, but got %d", expected, got)
	}
}
